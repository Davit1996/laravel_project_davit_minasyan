<!Doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Admin Profile</title>
    <link rel="stylesheet" href="{{URL::to('/css/layouts.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">
</head>
<body>
<header>
    @if(Auth::user()->profile==="Admin")
        <div><h3>A D M I N</h3></div>
        <div></div>
        <div><a href="{{URL::to('Admin')}}">Home</a></div>
        <div><a href="{{URL::to("Rezume")}}">All rezume</a></div>
        <div><a href="{{URL::to('User')}}">All user</a></div>
    @else
        <div><h3>U s e r</h3></div>
        <div></div>
        <div><a href="{{URL::to('Admin')}}">Home</a></div>
        <div><a href="{{URL::to('Rezume/create')}}">New rezume</a></div>
        <div></div>
    @endif
    <div><a href="{{URL::to('User/logout')}}">Log out</a></div>
</header>
<div id="app">
@yield("layouts")
</div>
<br>
<div id="app_footer">
    <footer-component></footer-component>
</div>
</body>
<script src="{{URL::to('/js/app.js')}}"></script>
</html>