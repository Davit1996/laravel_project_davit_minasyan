<!Doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>REZUME.am</title>
    <link rel="stylesheet" href="{{URL::to('/css/style.css')}}">
</head>
<body>
<br>
<form method="post" action="{{URL::to('Confirm/store')}}" class="form_registration" >
    <h1>Registration</h1>
    <div>
            <div class="registr_error">
                @if($errors->has("name"))
                    {{$errors->first("name")}}
                @endif
            </div>
        <input type="text" name="name" placeholder="Name" >
    </div>
    <div>
            <div class="registr_error">
                @if($errors->has("surname"))
                    {{$errors->first("surname")}}
                @endif
            </div>
        <input type="text" name="surname" placeholder="Surname" >
    </div>
    <div>
            <div class="registr_error">
                @if($errors->has("age"))
                    {{$errors->first("age")}}
                @endif
            </div>
        <input type="number" name="age" placeholder="Age" >
    </div>
    <div>
            <div class="registr_error">
                @if($errors->has("email"))
                    {{$errors->first("email")}}
                @endif
            </div>
        <input type="email" name="email" placeholder="Email" >
    </div>
    <div>
            <div class="registr_error">
                @if($errors->has("password"))
                    {{$errors->first("password")}}
                @endif
            </div>
        <input type="password" name="password" placeholder="Password" >
    </div>
    <div>
            <div class="registr_error">
                @if($errors->has("cnfpassword"))
                    {{$errors->first("cnfpassword")}}
                @endif
            </div>
        <input type="password" name="cnfpassword" placeholder="Confirm password" >
    </div>
    @csrf
    <button>Send</button>
</form>


</body>
</html>