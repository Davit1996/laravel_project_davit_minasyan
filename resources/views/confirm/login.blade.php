<!Doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>REZUME.am</title>
    <link rel="stylesheet" href="{{URL::to('/css/style.css')}}" >
</head>
<body>
<br>
<form method="post" action="{{URL::to('Confirm/enter')}}" class="form_registration">
    <h1>Login</h1>
    <div>
        <div class="registr_error">
            @if($errors->has("email"))
                {{$errors->first("email")}}
            @endif
        </div>
        <input type="email" name="email" placeholder="Email" >
    </div>
    <div>
        <div class="registr_error">
            @if($errors->has("password"))
                {{$errors->first("password")}}
            @endif
        </div>
        <input type="password" name="password" placeholder="Password" >
    </div>
    @csrf
    <button>Send</button>
</form>

</body>
</html>