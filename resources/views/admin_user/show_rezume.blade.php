@extends("layouts/layouts")
@section("layouts")
    <div class="area"></div>

    <div class="rezume_show">
        <div>
            <h2> F o t o ` </h2>
            <span><img src="{{URL::to('/storage/'.$Rezume[0]['image'])}}"></span>
        </div>
        <div>
            <h2> Name ` </h2>
            <span>{{$Rezume[0]["name"]}}</span>
        </div>
        <div>
            <h2> Surname ` </h2>
            <span>{{$Rezume[0]["surname"]}}</span>
        </div>
        <div>
            <h2> Age ` </h2>
            <span>{{$Rezume[0]["age"]}}</span>
        </div>
        <div>
            <h2> Specialty ` </h2>
            <span>{{$Rezume[0]["specialty"]}}</span>
        </div>
    </div>
    <div class="rezume_desciption">
        <div>
            <h2> Description ` </h2>
            <span>{{$Rezume[0]["description"]}}</span>
        </div>
        <div class="area"></div>
    </div>
    <div class="area"></div>
@endsection
