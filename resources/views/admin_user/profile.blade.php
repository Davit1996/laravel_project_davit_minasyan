@extends("layouts/layouts")
@section("layouts")
    <div class="data_profile">
        <div>
            <img src="{{URL::to('/storage/'.Auth::user()->image)}}" class="img_profile">
        </div>
        <div class="image_add">Add foto</div>
        <div class="upload_image_data">
            <h2>{{Auth::user()->name." ".Auth::user()->surname}}</h2>
        </div>
        <div class="img_profile_div">
            <form method="post" action="{{URL::to('image')}}" enctype="multipart/form-data">
                <div class="error_image">
                @if($errors->has("image"))
                        {{$errors->first("image")}}
                @endif
                </div>
                <input type="file" name="image">
                    @csrf
                <button>Send</button>
            </form>
        </div>
    </div>
    <div class="area"></div>
    <div id="app_footer">
        <footer-component></footer-component>
    </div>
@endsection