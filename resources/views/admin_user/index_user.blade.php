@extends("layouts/layouts")
@section("layouts")
<div class="area"></div>
    <div>
        @foreach($users as $user)
            <div>
                <div>
                    <img src="{{URL::to("/storage/".$user->image)}}" width="200px" height="200px">
                </div>
                <h2>{{$user->name." ".$user->surname}}</h2>
                <h3>{{$user->age}}</h3>
            </div>
            <hr>
        @endforeach
    </div>
<div class="area"></div>
<div id="app_footer">
    <footer-component></footer-component>
</div>
@endsection