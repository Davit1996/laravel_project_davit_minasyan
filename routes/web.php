<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::view("rezume/login","confirm/login");
Route::view("rezume/registration","confirm/registration");
Route::post("Confirm/store","ConfirmController@store");
Route::post("Confirm/enter","ConfirmController@enter");
Route::middleware("user_id")->group(function(){
    Route::get("User/logout","UserController@logout");
    Route::post("User/image","UserController@image");
    Route::post("image","AdminController@image");
    Route::resource("User","UserController");
    Route::resource("Rezume","RezumeController");
    Route::resource("Admin","AdminController");
});




