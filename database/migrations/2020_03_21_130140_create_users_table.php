<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->Increments("id");
            $table->string("name");
            $table->string("surname");
            $table->integer("age");
            $table->string("email");
            $table->string("password");
            $table->string("profile")->default("user");
            $table->string("image")->default("uploads/avatar.jpg");
            $table->string("second")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
