<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRezumeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rezume', function (Blueprint $table) {
            $table->Increments("id");
            $table->string("name");
            $table->string("surname");
            $table->integer("age");
            $table->string("phone");
            $table->string("specialty");
            $table->text("description");
            $table->string("image")->default("uploads/avatar.jpg");
            $table->integer("users_id")->unsigned();
            $table->foreign("users_id")->on("users")->references("id")->onUpdate("cascade")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rezume');
    }
}
