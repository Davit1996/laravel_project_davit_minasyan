<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Users;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Users::Insert([
           "name" => "Davit",
            "surname" => "Minasyan",
            "age" => "23",
            "profile" => "Admin",
            "email" => "jjbagda@mail.ru",
            "password" => Hash::make("0123456789"),
        ]);
        Users::Insert([
            "name" => "Hayk",
            "surname" => "Yousbashyan",
            "age" => "28",
            "profile" => "user",
            "email" => "Hayk@mail.ru",
            "password" => Hash::make("0123456789"),
        ]);
    }
}
