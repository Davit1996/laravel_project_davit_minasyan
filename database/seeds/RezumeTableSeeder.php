<?php

use Illuminate\Database\Seeder;
use App\Rezume;

class RezumeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        Rezume::create([
            "name" => "Hayk",
            "surname" => "Yousbashyan",
            "age" => 28,
            "phone" => +37410554749,
            "specialty" => "meneger",
            "description" => "hianali masnaget em",
            "users_id" => 2
        ]);
    }
}
