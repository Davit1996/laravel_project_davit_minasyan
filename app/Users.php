<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $all)
 * @method static Insert(array $array)
 */
class Users extends Model
{
    protected $table = "users";
    public $timestamp = false;
    protected $fillable = [
        'name',
        'surname',
        'age',
        'email',
        'password',
    ];
}
