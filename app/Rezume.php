<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $all)
 */
class Rezume extends Model
{
    protected $table = "rezume";
    public $timestamps = false;
    protected $fillable = [
        "name",
        "surname",
        "age",
        "phone",
        "specialty",
        "description",
        "users_id"
        ];

}
