<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\Rezume;
use App\Image;

class RezumeController extends Controller
{
    public function index(){
        $Rezume = Rezume::all();
        return view("admin_user/index_rezume",compact("Rezume"));
    }

    public function create(){
        return view("admin_user/create");
    }

    /**
     * @param Request $x
     */
    public function store(Request $x){
        $x["users_id"] = auth()->id();
        if(empty($x["image"])) {
            Rezume::create($x->all());
        } else{
            Rezume::create($x->all());
            $Rezume = Rezume::where(["users_id"=>auth()->id()])->get()->last();
            $x["rezume_id"] = $Rezume["id"];
            $z["image"] = $x["image"]->store("uploads","public");
            Rezume::where(["id"=>$Rezume["id"]])->update(["image"=>$z["image"]]);
        }
    }

    public function show($id){
        $Rezume  = Rezume::where(["id"=>$id])->get();
        return view("admin_user/show_rezume",compact("Rezume"));
    }

}
