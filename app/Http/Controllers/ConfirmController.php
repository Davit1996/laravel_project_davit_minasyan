<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserValidate;
use App\Http\Requests\LoginValidate;
use App\Users;
use Mail;


class ConfirmController extends Controller
{
    public function store(UserValidate $x){
        unset($x["cnfpassword"],$x["_token"]);
        $x["password"] = Hash::make($x["password"]);
        Users::create($x->all());
        return Redirect::to("/rezume/login");
    }
    public function enter(LoginValidate $x){
        unset($x["_token"]);
        if(Auth::attempt($x->all())){
            return Redirect::to("Admin");
        }else{
            return Redirect::to("/rezume/login")->withErrors(["email"=>"Incorrect login or password "]);
        }
    }

}
