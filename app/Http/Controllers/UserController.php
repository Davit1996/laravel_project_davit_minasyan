<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserValidate;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Users;
use App\View;

class UserController extends Controller
{

    public function index(){
        $users = Users::where(["profile"=>"user"])->get();
        return view("admin_user/index_user",compact("users"));
    }
    public function show(){
        $User = Users::where("id",auth()->id())->first();
        if($User["profile"] ==="user"){
            return view("admin_user/profile");
        } else{
            return view("admin_user/profile");
        }
    }

    public function logout(){
        Auth::logout();
        return Redirect::to("/rezume/login");
    }
}
