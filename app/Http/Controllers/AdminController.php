<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ImageValidate;
use App\Users;
use App\Image;


class AdminController extends Controller
{
    public function index(){
        return view("/admin_user/profile");
    }
    public function image(ImageValidate $x){
        $z["image"] = $x["image"]->store("uploads","public");
        Users::where(["id" => auth()->id()])->update(["image"=>$z["image"]]);
        return Redirect::to('/Admin');
    }
}
