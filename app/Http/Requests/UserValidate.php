<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|min:5|max:10",
            "surname" => "required|min:5|max:10",
            "age" => "required|numeric|min:18|max:50",
            "email" => "required|email|unique:users",
            "password" => "required|min:6|max:20",
            'cnfpassword' => 'required|required_with:password|same:password',
        ];
    }
}
